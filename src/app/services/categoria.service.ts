import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';

const apiUrl = environment.apiMarvel;
const key = '5ab01f97f6fb34e750ecad42f310cdf8';
const hash = 'fa2f92a69b5276d801a395eab9980875';
const ts = '1';
@Injectable({
  providedIn: 'root',
})
export class CategoriaService {
  constructor(private http: HttpClient) {}

  cargaCategoria(categoria: any) {
    const url = `https://gateway.marvel.com/v1/public/${categoria}?ts=${ts}&apikey=${key}&hash=${hash}&limit=100`;
    return this.http.get<any>(url).pipe(
      map((res) => {
        console.log(res);
        return res;
      })
    );
  }
}
