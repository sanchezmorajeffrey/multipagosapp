import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase/app';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { Storage } from '@ionic/storage-angular';
import { NavController } from '@ionic/angular';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private _storage: Storage | null = null;
  // eslint-disable-next-line @typescript-eslint/member-ordering
  public token: string = null;
  constructor(
    private auth: AngularFireAuth,
    private storage: Storage,
    private googlePlus: GooglePlus,
    private navCtrl: NavController
  ) {
    this.init();
  }

  async init() {
    const storage = await this.storage.create();
    // eslint-disable-next-line no-underscore-dangle
    this._storage = storage;
  }

  async loginGoogleAndroid() {
    const res = await this.googlePlus.login({
      webClientId:
        '743689351760-dmirqpq8ombq5j1nso1ce0ff56duc9ug.apps.googleusercontent.com',
      offline: true,
    });
    const resConfirmed = await this.auth.signInWithCredential(
      firebase.auth.GoogleAuthProvider.credential(res.idToken)
    );
    const user = resConfirmed.user;
    this.guardarToken(user.refreshToken);
    return user;
  }

  async loginGoogleWeb() {
    const res = await this.auth.signInWithPopup(
      new firebase.auth.GoogleAuthProvider()
    );
    const user = res.user;
    this.guardarToken(user.refreshToken);
    return user;
  }

  async guardarToken(token: string) {
    await this.storage.create();
    this.token = token;
    await this.storage.set('token', token);
    await this.validaToken();
  }

  async cargarToken() {
    this.token = (await this.storage.get('token')) || null;
  }

  async validaToken(): Promise<boolean> {
    await this.cargarToken();
    if (!this.token) {
      this.navCtrl.navigateRoot('/login');
      return Promise.resolve(false);
    } else {
      return Promise.resolve(true);
    }
  }

  logout() {
    this.auth.signOut();
    this.storage.clear();
    this.navCtrl.navigateRoot('/login');
  }
}
