import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoriaService } from 'src/app/services/categoria.service';
import { IonSegment } from '@ionic/angular';
@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page implements OnInit {
  @ViewChild(IonSegment, { static: true }) segment: IonSegment;
  categorias = [
    'characters',
    'comics',
    'creators',
    'events',
    'series',
    'stories',
  ];
  datos: any[] = [];
  habilitado = true;
  constructor(private marvel: CategoriaService) {}
  ngOnInit() {
    this.segment.value = this.categorias[0];
    this.cargarData(this.segment.value);
  }

  cargarCategoria(event) {
    this.datos = [];
    this.cargarData(event.detail.value);
  }

  cargarData(categoria: string) {
    this.marvel.cargaCategoria(categoria).subscribe((res) => {
      this.datos = res.data.results;
      console.log(this.datos);
    });
  }
}
