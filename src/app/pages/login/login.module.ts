import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';
import { environment } from '../../../environments/environment';
import { LoginPage } from './login.page';


import { AngularFirestore } from '@angular/fire/firestore';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LoginPageRoutingModule,


  ],
  declarations: [LoginPage], providers: [AngularFirestore]
})
export class LoginPageModule {}
