import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { AuthService } from 'src/app/services/auth.service';
import { Platform } from '@ionic/angular';
import { Router, ActivatedRoute } from '@angular/router';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  @ViewChild('slidePrincipal', { static: true }) slides: IonSlides;
  returnUrl: string;

  slideOpts = {
    allowSlidePrev: false,
    allowSlideNext: false,
  };

  constructor(
    private auth: AuthService,
    public platform: Platform,
    public router: Router,
    private route: ActivatedRoute,
    private navCtrl: NavController
  ) {}

  ngOnInit() {
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '';
  }

  loginGoogle() {
    if (this.platform.is('android')) {
      this.auth.loginGoogleAndroid().then((res) => {
        console.log('login', res);
        this.navCtrl.navigateRoot([this.returnUrl], { animated: true });
      });
    } else {
      this.auth.loginGoogleWeb().then((res) => {
        console.log('login', res);
        this.navCtrl.navigateRoot([this.returnUrl], { animated: true });
      });
    }
  }
}
