import { Component, OnInit, ViewChild } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { AuthService } from 'src/app/services/auth.service';

declare let require: any;

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss'],
})
export class Tab3Page implements OnInit {
  @ViewChild('mapa', { static: true }) mapa;
  cargandoGeo = false;
  tempImages: string[] = [];
  user: any = {
    coords: null,
    posicion: false,
  };
  private win: any = window;
  constructor(
    private geoLocation: Geolocation,
    private camera: Camera,
    private auth: AuthService
  ) {}

  ngOnInit() {}

  getGeo() {
    if (!this.user.posicion) {
      this.user.coords = null;
      return;
    }

    this.cargandoGeo = true;

    this.geoLocation
      .getCurrentPosition()
      .then((resp) => {
        this.cargandoGeo = false;
        const coords = `${resp.coords.latitude},${resp.coords.longitude}`;
        console.log(coords);
        this.user.coords = coords;
        const latLng = this.user.coords.split(',');
        const lat = Number(latLng[0]);
        const lng = Number(latLng[1]);

        const mapboxgl = require('mapbox-gl/dist/mapbox-gl.js');
        mapboxgl.accessToken =
          'pk.eyJ1IjoiamVmcnlzYW5jaGV6IiwiYSI6ImNrYXNvaDdnMzBsb24zNWt5aHVjYmxybGwifQ.jqAbgMIcE8OUm0-S-r2zqA';
        const map = new mapboxgl.Map({
          container: this.mapa.nativeElement,
          center: [lng, lat],
          zoom: 15,
          style: 'mapbox://styles/mapbox/streets-v11',
        });

        const marker = new mapboxgl.Marker().setLngLat([lng, lat]).addTo(map);
      })
      .catch((error) => {
        this.cargandoGeo = false;
      });

    console.log(this.user);
  }

  camara() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.CAMERA,
    };
    this.procesarImagen(options);
  }

  procesarImagen(options: CameraOptions) {
    this.camera.getPicture(options).then(
      (imageData) => {
        // imageData is either a base64 encoded string or a file URI
        // If it's base64 (DATA_URL):
        const img = this.win.Ionic.WebView.convertFileSrc(imageData);
        console.log(img);
        this.tempImages.push(img);
      },
      (err) => {
        // Handle error
      }
    );
  }

  libreria() {
    const options: CameraOptions = {
      quality: 60,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      correctOrientation: true,
      sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
    };
    this.procesarImagen(options);
  }

  logout() {
    this.auth.logout();
  }
}
