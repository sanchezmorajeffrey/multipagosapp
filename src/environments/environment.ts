// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: 'AIzaSyAlgqGoDYfdQdH-MHRV5dkXCcBYwbh_jz4',
    authDomain: 'multipagos-3e846.firebaseapp.com',
    projectId: 'multipagos-3e846',
    storageBucket: 'multipagos-3e846.appspot.com',
    messagingSenderId: '743689351760',
    appId: '1:743689351760:web:f1f66fdde5bd6dc3a11755',
    measurementId: 'G-X4ML88GRP3',
  },

  apiMarvel: 'https://gateway.marvel.com/docs/public',
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
